package practice;

public class PyramidDemo1 {
	static void getPyramidDemo1() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*"+ " ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo1 is completed:");
	}

	static void getPyramidDemo10() {
		for (int i = 0; i < 5; i++) {
			for (int j = 5; j > i; j--) {
				System.out.print(j+" ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo10 is completed:");
	}

	static void getPyramidDemo11() {
		for (int k = 4, i = 1; i < 6; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + k+ " ");

			}
			System.out.println();
			k = k--;

		}
		System.out.println("getPyramidDemo11 is completed:");
	}

	static void getPyramidDemo12() {
		for (int k = 4, i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + k +" ");

			}
			System.out.println();
			k = k - 1;
		}
		System.out.println("getPyramidDemo12 is completed:");
	}

	static void getPyramidDemo2() {
		int odd=7;
		int noOfSpace=1;
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= odd; j++) {
				System.out.print("*"+ " ");
			}
			odd=odd-2;
			System.out.println();
			for (int j = 1; j <= noOfSpace; j++) {
				System.out.print(" ");
			}
			if(noOfSpace<5){
			noOfSpace=noOfSpace+2;
			}
		}
		System.out.println("getPyramidDemo2 is completed:");
	}

	static void getPyramidDemo3() {
		for (int i = 1; i <= 5; i++) {
			for (int j = i; j <= 5; j++) {
				System.out.print(j+" ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo3 is completed:");
	}

	static void getPyramidDemo4() {
		for (int i = 5; i > 0; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo4 is completed:");
	}

	static void getPyramidDemo5() {
		for (int i = 5; i > 0; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo5 is completed:");
	}

	static void getPyramidDemo6() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(i+" ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo6 is completed:");
	}

	static void getPyramidDemo7() {
		for (int i = 1; i < 6; i++) {

			for (int j = i; j > 1; j--) {
				System.out.print(j - 1+ " ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo7 is completed:");
	}

	static void getPyramidDemo8() {
		for (int i = 1; i < 6; i++) {
			for (int k = 1; k <= 1; k++) {
				System.out.print(k);
			}
			for (int j = i; j > 1; j--) {
				System.out.print((j - 1)+" ");
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo8 is completed:");
	}

	static void getPyramidDemo9() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j);
				if (i != 1) {
					if (j == i) {
						for (int k = 0; k < (j - 1); k++) {
							System.out.print(j - (k + 1));
							System.out.print(" ");
						}
					}
				}
			}
			System.out.println();
		}
		System.out.println("getPyramidDemo9 is completed:");
	}

	public static void main(String[] args) {
		getPyramidDemo1();
		getPyramidDemo2();
		getPyramidDemo3();
		getPyramidDemo4();
		getPyramidDemo5();
		getPyramidDemo6();
		getPyramidDemo7();
		getPyramidDemo8();
		getPyramidDemo9();
		getPyramidDemo10();
		getPyramidDemo11();
		getPyramidDemo12();
	}


}
