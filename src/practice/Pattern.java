package practice;

public class Pattern {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("################################################################");
		patternOne();
		System.out.println("################################################################");
		patternTwo(5);
		System.out.println("################################################################");
		patternThree();
		System.out.println("################################################################");
		patternFour();
		System.out.println("################################################################");
		patternFive();
		System.out.println("################################################################");
		patternSix(2, 5);
		System.out.println("################################################################");
		patternSeven();
		System.out.println("################################################################");

		pattern8();
		pattern9();
		pattern10();
		pattern11();
		pattern12();
		pattern13();
		pattern14();
		

	}

	private static void pattern10() {

		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {

				if ((i + j) % 2 == 0) {
					System.out.print("1");
				} else {
					System.out.print("0");
				}
			}
			System.out.println();

		}
		System.out.println("pattern10 is completed:");
	}

	private static void pattern11() {
		int k = 0;
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {

				System.out.print("\t" + (j + k));
			}
			System.out.println();
			k = k + i;
		}
		System.out.println("pattern11 is completed:");
	}

	private static void pattern12() {
		int k = 0;
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {
				k++;
				System.out.print("\t" + k);
			}
			System.out.println();

		}
		System.out.println("pattern12 is completed:");
	}

	private static void pattern13() {
		int k = 1;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < (5 - i); j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < i; j++) {
				System.out.print(k);
				k = k * (i - j) / (j + 1);
			}
			System.out.println();

		}
		System.out.println("pattern13 is completed:");
	}

	private static void pattern14() throws InterruptedException {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(j);
				Thread.sleep(1500);
			}
			System.out.println();

		}
		System.out.println("pattern14 is completed:");
	}

	private static void pattern8() {

		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {
				System.out.print(i);
			}
			System.out.println();

		}
		System.out.println("pattern8 is completed:");
	}

	private static void pattern9() {

		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {
				System.out.print(j);
			}
			System.out.println();

		}

		System.out.println("pattern9 is completed:");
	}

	private static void patternFive() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("\t* ");
			}
			System.out.println();
			System.out.println();
			System.out.println();
		}
		System.out.println("patternFive is completed:");
	}

	private static void patternFour() {
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= 4; j++) {
				if (i == 1 || i == 4 || j == 1 || j == 4) {
					System.out.print("\t*");
				} else {
					System.out.print("\t ");
				}
			}
			System.out.println();
			System.out.println();
			System.out.println();
		}
		System.out.println("patternFour is completed:");
	}

	private static void patternOne() {
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j);
			}
			System.out.println();

		}
		System.out.println("patternOne is completed:");
	}

	private static void patternSeven() {

		/**
		 * 1 121 12321 1234321
		 */

		int odd = 1;
		for (int i = 1; i <= 4; i++) {
			int k = 0;
			for (int j = 1; j <= odd; j++) {

				if (j <= i) {
					k++;
				} else {
					k--;
				}
				System.out.print(k);
			}
			System.out.println();
			odd = odd + 2;
		}
		System.out.println("patternSeven is completed:");
	}

	private static void patternSix(int start, int n) {
		for (int i = 1; i <= n; i++) {

			for (int j = 1; j <= i; j++) {
				System.out.print(start);
			}
			System.out.println();
			start++;
		}
		for (int i = 1; i <= n; i++) {
			start--;
			for (int j = 1; j <= (n + 1) - i; j++) {
				System.out.print(start);
			}
			System.out.println();

		}
		System.out.println("patternSix is completed:");
	}

	private static void patternThree() {
		int odd = 1;
		int noOfSpace = 4;
		for (int i = 1; i <= 5; i++) {
			noOfSpace--;
			for (int j = 1; j <= noOfSpace; j++) {
				System.out.print(" ");
			}
			for (int j = 1; j <= odd; j++) {
				System.out.print(j);
			}
			System.out.println();
			odd = odd + 2;

		}
		System.out.println("patternThree is completed:");
	}

	private static void patternTwo(int num) {
		int odd = 1;
		int noOfSpace = num - 1;
		for (int i = 1; i <= num; i++) {
			noOfSpace--;
			for (int j = 1; j <= noOfSpace; j++) {
				System.out.print("A");
			}
			int k = 0;
			for (int j = 1; j <= odd; j++) {

				if (j <= i) {
					k = k + 1;
				} else {
					k = k - 1;
				}

				System.out.print(k);
			}
			System.out.println();
			odd = odd + 2;

		}
		System.out.println("patternTwo is completed:");
	}

	}

